<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/generatePDF', 'IndexController@generatePDF')->name('generatePDF');
Route::post('/savePDF', 'IndexController@savePDF')->name('savePDF');
Route::post('/saveForm', 'IndexController@saveForm')->name('saveForm');
Route::get('/createForm/{id?}', 'IndexController@createForm')->name('createForm');
Route::get('/fillForm/{id}', 'IndexController@fillForm')->name('fillForm');
Route::get('/step2/{id}', 'IndexController@step2')->name('step2');
Route::get('/step3/{id}', 'IndexController@step3')->name('step3');
Route::post('/step2/{id}', 'IndexController@step2Process')->name('step2Process');
Route::get('/generateForm/{id}', 'IndexController@generateForm')->name('generateForm');
Route::get('/downloadPDF/{id}', 'IndexController@downloadPDF')->name('downloadPDF');
Route::get('/driverLicense', function() {
	return view('printPDFDriverLicense');
});
