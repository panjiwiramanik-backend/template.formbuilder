<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FormField extends Model
{
    protected $table = 'form_field';

    protected $fillable = [
        'form_id', 
        'label', 
        'field', 
        'form_type', 
        'value'
    ];
}
