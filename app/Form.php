<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Form extends Model
{
    protected $table = 'form';

    protected $fillable = [
        'form_name', 
        'form_template', 
        'forms', 
        'forms_json', 
        'file_generated'
    ];
}
