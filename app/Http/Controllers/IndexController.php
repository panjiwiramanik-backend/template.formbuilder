<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class IndexController extends Controller
{
	public function generateForm($id) {
		try {
			$form = \App\Form::find($id);
			$formField = \App\FormField::where('form_id', $id)->get()->toArray();

			$lengthData = count($formField) - 1;
			$formData = '{';
			foreach ($formField as $key => $field) {
				if ($field['value']) {
					$value = $field['value'];
					if ($this->isJson($field['value'])) {
						$value = json_decode($field['value'])[0];
					}
					if (\DateTime::createFromFormat('Y-m-d', $value) !== FALSE) {
						$value = date("d F Y", strtotime($value));
					}
					$formData .= '"' . $field['field'] . '":"' . $value . '"';
					if ($key < $lengthData) {
						$formData .= ',';
					}
				}
			}
			$formData .= '}';
			$data['formData'] = json_decode($formData);

			$filename = $this->generateRandomString();
			$pdf = PDF::loadHTML(view('printPDFDriverLicense', $data))
						->setOption('enable-javascript', true)
						->setOption('no-stop-slow-scripts', true)
						->setOption('enable-smart-shrinking', true)
						->setOption('javascript-delay', 1000)
						->setPaper('A4');
			$pdf->save(base_path('public/doc/' . $filename . '.pdf'));

			$form->file_generated = $filename . '.pdf';
			$form->save();
		} catch (\Exception $e) {
			dd($e);
		}
	}
	public function downloadPDF($id) {
		try {
			$form = \App\Form::find($id);

			return response()->file(public_path('doc/' . $form->file_generated));
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public function savePDF(Request $request) {
		try {
			if (!$request->id) {
				$form = new \App\Form;
				$form->form_name = $request->form_name;
				$form->forms = $request->formData;
				$form->forms_json = json_encode(preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $request->json)));
				$form->save();

				foreach (json_decode($request->json) as $json) {
					$formField = new \App\FormField;
					$formField->form_id = $form->id;
					$formField->field = $json->name;
					$formField->label = $json->label;
					$formField->form_type = $json->type;
					$formField->save();
				}

				return response(['status' => 'success'], 200);
			} else {
				$form = \App\Form::find($request->id);
				$form->form_name = $request->form_name;
				$form->forms = $request->formData;
				$form->forms_json = json_encode(preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $request->json)));
				$form->save();

				\App\FormField::where('form_id', '=', $request->id)->delete();

				foreach (json_decode($request->json) as $json) {
					$formField = new \App\FormField;
					$formField->form_id = $form->id;
					$formField->field = $json->name;
					$formField->label = $json->label;
					$formField->form_type = $json->type;
					$formField->save();
				}

				return response(['status' => 'success'], 200);
			}
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function saveForm(Request $request) {
		try {
			$form = \App\Form::find($request->formId);

			if ($request->action == '1') {
				$form->forms_json = json_encode(preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $request->userData)));
				$form->save();

				$formField = \App\FormField::where('form_id', '=', $request->formId)->get();
				foreach (json_decode($request->userData) as $json) {
					foreach ($formField as $field) {
						if ($json->name == $field->field) {
							if ($field->form_type != 'file') {
								$field->value = json_encode($json->userData);
								$field->save();
							} else {
								if ($request->hasFile($field->field)) {
									$string = $this->generateRandomString();
									$request->file($field->field)->move(public_path('images'), $string . '.' . $request->file($field->field)->getClientOriginalExtension());
									$field->value = $string . '.' . $request->file($field->field)->getClientOriginalExtension();
									$field->save();
								}
							}
						}
					}
				}

				$this->generateForm($request->formId);
			} else {
				$filename = $this->generateRandomString();
				move_uploaded_file($request->file('pdfOutput')->getPathName(), base_path('public/doc/' . $filename . '.pdf'));

				$form->file_generated = $filename . '.pdf';
				$form->save();
			}

			return response(['status' => 'success'], 200);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function step2(Request $request) {
		try {
			$data['form'] = \App\Form::find($request->id);
			$data['formField'] = \App\FormField::where('form_id', '=', $request->id)->get();
			return view('step2', $data);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function step3(Request $request) {
		try {
			$data['form'] = \App\Form::find($request->id);
			$data['formField'] = \App\FormField::where('form_id', '=', $request->id)->get();
			return view('step3', $data);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function step2Process(Request $request) {
		try {
			$form = \App\Form::find($request->formId);
			$filename = $this->generateRandomString();
			move_uploaded_file($request->file('pdfOutput')->getPathName(), base_path('public/doc/' . $filename . '.pdf'));

			$form->file_generated = $filename . '.pdf';
			$form->save();

			return response(['status' => 'success'], 200);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function generateRandomString($length = 10) {
	    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
	}

	public function createForm($id = null) {
		$data = [];
		if ($id) {
			$data['form'] = \App\Form::find($id);
		}
		return view('createForm', $data);
	}

	public function fillForm($id) {
		$data['form'] = \App\Form::find($id);
		$data['formField'] = \App\FormField::where('form_id', '=', $id)->get();
		return view('fillForm', $data);
	}
}
