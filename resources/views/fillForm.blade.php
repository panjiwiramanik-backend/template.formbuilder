<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

        <style>
            canvas {
                display: none;
            }
            #container {
                width: 21cm !important;
                height: 29.7cm !important;
            }
        </style>

    </head>
    <body class="p-5">
        <a class="text-white btn btn-danger mb-5" href="{{ url('/') }}"><- Back</a>
        <h1>STEP 1 - Fill Form <b>{{ $form->form_name }}</b></h1>
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('doc/license_sohvLNP.jpg') }}" width="100%" height="auto">
            </div>
            <div class="col-md-6">
                <div id="render">
                </div>

                {{-- @foreach($formField as $field)
                    @if($field->form_type == 'file')
                        <h3>{{$field->label}}</h3>
                        <img src="{{ asset('images/' . $field->value) }}" alt="{{ $field->label }}" width="200px" height="auto">
                    @endif
                @endforeach --}}
                <br>
                <br>
                <br>
                <button class="btn btn-success" id="saveData">Next Step</button>
                {{-- <button class="btn btn-success" id="savePdf">Save PDF</button> --}}
                {{-- <button class="btn btn-success ml-3" id="printPDF">Print PDF</button> --}}
            </div>
        </div>

        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/form-builder.js') }}"></script>
        <script src="{{ asset('js/form-render.js') }}"></script>
        <script src="{{ asset('js/pdfjs/pdf.js') }}"></script>
        <script src = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
        <script src="https://unpkg.com/konva@7.2.0/konva.min.js"></script>

        <script>
            var reader = new FileReader();

            let imageUpload = [];
            let formData = JSON.stringify({!! json_decode($form->forms_json) !!});
            $('#render').formRender({formData});

            $("#saveData").on('click', function() {
                let userData = $('#render').formRender('userData');

                $("#saveData").attr("disabled", true)
                setTimeout(() => {
                    formDataSend = new FormData();
                    formDataSend.append('formId', {{$form->id}});
                    formDataSend.append('userData', JSON.stringify(userData));
                    formDataSend.append('action', 1);

                    imageUpload.forEach((y) => {
                        formDataSend.append(y.name, y.file);
                    })

                    $.ajax({
                        cache: false,
                        contentType: false,
                        processData: false,
                        url: '{{route('saveForm')}}',
                        type: 'POST',
                        data: formDataSend,
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        success: function(result) {
                            $("#saveData").attr("disabled", false)
                            window.location.href = '{{ route("step2", [$form->id]) }}';
                            return false;
                        },
                        error: function(result) {
                            $("#saveData").attr("disabled", false)
                        }
                    });
                }, 200)
            })

        </script>
    </body>
</html>
