<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    </head>
    <body class="p-5">
        <div>
            @foreach($formField as $f)
                {{$f->label}} = {{ json_decode($f->value)[0] ?? '' }} <br>
            @endforeach
            {{-- @if(count($imageUpload) > 0)
                @foreach($imageUpload as $i)
                    <img src="{{ $i }}" alt="1" width="200px" height="auto">
                @endforeach
            @endif --}}
        </div>

        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/form-builder.js') }}"></script>
        <script src="{{ asset('js/form-render.js') }}"></script>
    </body>
</html>
