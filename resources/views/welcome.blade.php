<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    </head>
    <body class="p-5">
        <a class="text-white btn btn-primary mb-5" href="{{ route('createForm') }}">Create Form</a>

        <table border="1">
            <tr>
                <th>Form Name</th>
                <th>Action</th>
            </tr>
            @php
                $form = \App\Form::all();
            @endphp
            @forelse($form as $f)
            <tr>
                <td>{{ $f->form_name }}</td>
                <td><a class="text-white btn btn-warning" href="{{ route('createForm', [$f->id]) }}">Edit Form</a> <a class="text-white btn btn-primary" href="{{ route('fillForm', [$f->id]) }}">Fill Form</a> <a class="text-white btn btn-success" href="{{ route('downloadPDF', [$f->id]) }}" target="_blank">Download PDF</a></td>
            </tr>
            @empty
            <tr>
                <td colspan="2">No Form</td>
            </tr>
            @endforelse
        </table>
    </body>
</html>
