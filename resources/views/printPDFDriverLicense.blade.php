<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <style>
            .header {
                text-align: left;
                border-bottom: 0px solid white !important;
            }

            th {
                padding: 10px !important;
            }

            div.page {
                page-break-after: always !important;
                page-break-inside: avoid !important;
            }
        </style>
    </head>
    <body>
        <div class="page">
            <div class="row">
                <div class="col-md-6">LEXIGO Global Pty Ltd <br>Certified Translation from Chinese to English</div>
                <div class="col-md-6 text-right">Page 1 of 2</div>
            </div>

            <div class="row py-3">
                <div class="col-md-12">
                    <table border="1" style="border-collapse: collapse;background-color: #ececec;border: 1px solid black;" width="100%">
                        <tr>
                            <th style="text-align: center;">
                                <h1>TRANSLATION OF<br>Drivers License of the People's Republic of China</h1>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table border="1" style="border-collapse: collapse;" width="100%">
                        <tr class="header">
                            <th>LICENSE NUMBER</th>
                            <th colspan="2">ISSUING AUTHORITY</th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">{{ $formData->license_number ?? '-' }}</th>
                            <th colspan="2">{{ $formData->issuing_authority ?? '-' }}</th>
                        </tr>
                        <tr class="header">
                            <th>DATE OF FIRST ISSUE</th>
                            <th>VALID FROM</th>
                            <th>VALID FOR</th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">{{ $formData->data_of_first_issue ?? '-' }}</th>
                            <th style="text-align: center;">{{ $formData->valid_from ?? '-' }}</th>
                            <th style="text-align: center;">{{ $formData->valid_for ?? '-' }}</th>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row py-3">
                <div class="col-md-12">
                    <h1 style="text-align: center;">PARTICULARS OF BEARER</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table border="1" style="border-collapse: collapse;" width="100%">
                        <tr class="header">
                            <th>GIVEN NAME(S)</th>
                            <th colspan="2">FAMILY NAME(S)</th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">{{ $formData->given_name ?? '-' }}</th>
                            <th style="text-align: center;" colspan="2">{{ $formData->family_name ?? '-' }}</th>
                        </tr>
                        <tr class="header">
                            <th colspan="3">ADDRESS</th>
                        </tr>
                        <tr>
                            <th colspan="3">{{ $formData->address ?? '-' }}</th>
                        </tr>
                        <tr class="header">
                            <th>NATIONALITY</th>
                            <th>DATE OF BIRTH</th>
                            <th>SEX</th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">{{ $formData->nationality ?? '-' }}</th>
                            <th style="text-align: center;">{{ $formData->date_of_birth ?? '-' }}</th>
                            <th style="text-align: center;">{{ $formData->sex ?? '-' }}</th>
                        </tr>
                        <tr class="header">
                            <th colspan="3">TYPE OF LICENSE</th>
                        </tr>
                        <tr>
                            <th colspan="3">{{ $formData->type_of_license ?? '-' }}</th>
                        </tr>
                    </table>
                </div>
            </div>

            <hr class="my-3" style="border: 1px solid black">
        </div>
    </body>
</html>
