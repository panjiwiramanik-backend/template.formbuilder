<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

        <style>
            .required-wrap, .description-wrap, .placeholder-wrap, .className-wrap, .access-wrap, .requireValidOption-wrap, .other-wrap, .inline-wrap, .toggle-wrap, .subtype-wrap, .style-wrap, .value-wrap, .multiple-wrap, .min-wrap, .max-wrap, .step-wrap, .maxlength-wrap, .rows-wrap, .frmb-control .formbuilder-icon-autocomplete, .frmb-control .formbuilder-icon-button, .frmb-control .formbuilder-icon-hidden, .frmb-control .formbuilder-icon-hidden, .frmb-control .formbuilder-icon-number, .frmb-control .formbuilder-icon-header, .frmb-control .formbuilder-icon-paragraph, .get-data, .save-template {
                display: none !important;
            }
        </style>
    </head>
    <body class="p-5">
        <a class="text-white btn btn-danger mb-5" href="{{ url('/') }}"><- Back</a>
        <h1>Form Name</h1>
        <input type="text" name="form_name" id="formName" value="{{ $form->form_name ?? '' }}">
        <h1>Editor</h1>
        <div id="editor"></div>
        <form id="render" style="display: none !important;"></form>
        <button id="savePDF" class="btn btn-success pt-1">Save Form</button>

        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/form-builder.js') }}"></script>
        <script src="{{ asset('js/form-render.js') }}"></script>

        <script>
            jQuery($ => {
                let imageUpload = [];
                // let formBuilder = $(document.getElementById('editor')).formBuilder({
                //     onAddField: function() {
                //         $("input[type='file']").on('change', function() {
                //             imageUpload.push(this.files[0])
                //         })
                //     },
                // });
                @if(@$form)
                let formBuilder = $(document.getElementById('editor')).formBuilder({
                    formData: JSON.stringify({!! json_decode($form->forms_json) !!})
                });
                @else
                    let formBuilder = $(document.getElementById('editor')).formBuilder();
                @endif
                let formRender = $(document.getElementById('render'));

                document.addEventListener('fieldRemoved', (param) => {
                    console.log(param);
                });

                $("#savePDF").on('click', function() {
                    let data = $(document.getElementById('editor')).formBuilder("getData", "json", true);
                    var formRenderOpts = {
                        formData: data,
                        dataType: 'json'
                    }
                                     
                    $(formRender).formRender(formRenderOpts);
                    let html = $(formRender).formRender("html");

                    $("#savePDF").attr("disabled", true)
                    setTimeout(() => {
                        formData = new FormData();

                        imageUpload.forEach((y) => {
                            formData.append('file[]', y);
                        })
                        formData.append('form_name', $("#formName").val());
                        formData.append('formData', html);
                        formData.append('json', data);

                        @if(@$form)
                        formData.append('id', {{$form->id}});
                        @endif

                        $.ajax({
                            cache: false,
                            contentType: false,
                            processData: false,
                            url: '{{route('savePDF')}}',
                            type: 'POST',
                            data: formData,
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            success: function(result) {
                                $("#savePDF").attr("disabled", false)
                                // var win = window.open('{{ asset("storage") }}' + '/' + result.name);
                                window.location.href = '{{ url("/") }}';
                                return false;
                            },
                            error: function(result) {
                                $("#savePDF").attr("disabled", false)
                            }
                        });
                    }, 200)
                })
            });

        </script>
    </body>
</html>
