<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

        <style>
            canvas {
                display: none;
            }
            #container {
                width: 21cm !important;
                height: auto;
            }
            #containerFalse {
                position: absolute;
                left: -999px;
                width: 21cm !important;
                height: 29.7cm !important;
            }
        </style>

    </head>
    <body class="p-5">
        <div id="containerFalse"></div>
        <a class="text-white btn btn-danger mb-5" href="{{ route('fillForm', [$form->id]) }}"><- Back</a>
        <h1>STEP 2 - Upload Stamp & Signature <b>{{ $form->form_name }}</b></h1>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="stamp">Stamp</label>
                    <input type="file" class="form-control" name="stamp" access="false" multiple="false" id="stamp">
                </div>
                <div class="form-group">
                    <label for="signature">Signature</label>
                    <input type="file" class="form-control" name="signature" access="false" multiple="false" id="signature">
                </div>
                <br>
                <button class="btn btn-success" id="savePdf">Next Step</button>
            </div>
            <div class="col-md-6">
                <div id="container" class="mx-auto"></div>
            </div>
        </div>

        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/form-builder.js') }}"></script>
        <script src="{{ asset('js/form-render.js') }}"></script>
        <script src="{{ asset('js/pdfjs/pdf.js') }}"></script>
        <script src = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
        <script src="https://unpkg.com/konva@7.2.0/konva.min.js"></script>

        <script>
            var reader = new FileReader();
            var sizeContainer = document.getElementById('containerFalse').getBoundingClientRect()
            var width = sizeContainer.width;
            var height = sizeContainer.height;
            var pdfImage = [];
            var pdfCreatedImage = [];

            let imageUpload = [];
            $("#stamp").on('change', function() {
                let index = imageUpload.findIndex((x) => x.name == 'stamp');
                if (index >= 0) {
                    imageUpload.splice(index, 1);
                    imageUpload.push({
                        name: 'stamp',
                        file: this.files[0]
                    })
                    reader.readAsDataURL(this.files[0]);
                    reader.onload = function () {
                        addImage(reader.result, 0, 0, 100, 100, true)
                    };
                } else {
                    imageUpload.push({
                        name: 'stamp',
                        file: this.files[0]
                    })
                    reader.readAsDataURL(this.files[0]);
                    reader.onload = function () {
                        addImage(reader.result, 0, 0, 100, 100, true)
                    };
                }
            })
            $("#signature").on('change', function() {
                let index = imageUpload.findIndex((x) => x.name == 'signature');
                if (index >= 0) {
                    imageUpload.splice(index, 1);
                    imageUpload.push({
                        name: 'signature',
                        file: this.files[0]
                    })
                    reader.readAsDataURL(this.files[0]);
                    reader.onload = function () {
                        addImage(reader.result, 0, 0, 100, 100, true)
                    };
                } else {
                    imageUpload.push({
                        name: 'signature',
                        file: this.files[0]
                    })
                    reader.readAsDataURL(this.files[0]);
                    reader.onload = function () {
                        addImage(reader.result, 0, 0, 100, 100, true)
                    };
                }
            })

            var pdfjsLib = window['pdfjs-dist/build/pdf'];
            pdfjsLib.GlobalWorkerOptions.workerSrc = '{{ asset("js/pdfjs/pdf.worker.js") }}';
            var loadingTask = pdfjsLib.getDocument('{{ asset("doc/" . $form->file_generated) }}');
            loadingTask.promise.then(function(pdf) {
                for (let i = 0;i < pdf.numPages;i++) {
                    pdf.getPage(i + 1).then(function(page) {
                        var canvas = document.createElement('canvas');
                        var context = canvas.getContext('2d');
                        var scale = 1.5;
                        var viewport = page.getViewport({scale: scale});

                        canvas.height += viewport.height;
                        canvas.width = viewport.width;

                        var renderContext = {
                            canvasContext: context,
                            viewport: viewport
                        };

                        var renderTask = page.render(renderContext);
                        renderTask.promise.then(function () {
                            pdfImage.push({position: i, img: canvas.toDataURL('image/jpeg', 1)});
                        });
                    });
                }

                setTimeout(() => {
                    loadKonva();
                    setTimeout(() => {
                        loadImagePDF();
                    }, 100)
                }, pdf.numPages * 300)
            }, function (reason) {
                console.error(reason);
            });

            function update(activeAnchor) {
                var group = activeAnchor.getParent();

                var topLeft = group.get('.topLeft')[0];
                var topRight = group.get('.topRight')[0];
                var bottomRight = group.get('.bottomRight')[0];
                var bottomLeft = group.get('.bottomLeft')[0];
                var image = group.get('Image')[0];

                var anchorX = activeAnchor.getX();
                var anchorY = activeAnchor.getY();

                // update anchor positions
                switch (activeAnchor.getName()) {
                    case 'topRight':
                        topLeft.y(anchorY);
                        bottomRight.x(anchorX);
                    break;
                    case 'bottomRight':
                        bottomLeft.y(anchorY);
                        topRight.x(anchorX);
                    break;
                    case 'bottomLeft':
                        bottomRight.y(anchorY);
                        topLeft.x(anchorX);
                    break;
                }

                image.position(topLeft.position());

                var width = topRight.getX() - topLeft.getX();
                var height = bottomLeft.getY() - topLeft.getY();
                if (width && height) {
                    image.width(width);
                    image.height(height);
                }
            }

            function addAnchor(group, x, y, name) {
                var stage = group.getStage();
                var layer = group.getLayer();

                if (name == 'topRight') {
                    var anchor = new Konva.Circle({
                        x: x,
                        y: y,
                        stroke: '#666',
                        fill: '#FF0000',
                        strokeWidth: 2,
                        radius: 8,
                        name: name,
                        draggable: true,
                        dragOnTop: false
                    });
                } else {
                    var anchor = new Konva.Circle({
                        x: x,
                        y: y,
                        stroke: '#666',
                        fill: '#ddd',
                        strokeWidth: 2,
                        radius: 8,
                        name: name,
                        draggable: true,
                        dragOnTop: false
                    });
                }

                anchor.on('click', function () {
                    document.body.style.cursor = 'default';
                    if (this.getName() == 'topRight') {
                        group.destroy();
                        layer.draw();
                    }
                });

                anchor.on('dragmove', function () {
                    update(this);
                    layer.draw();
                });
                anchor.on('mousedown touchstart', function () {
                    group.draggable(false);
                    this.moveToTop();
                });
                anchor.on('dragend', function () {
                    group.draggable(true);
                    layer.draw();
                });

                anchor.on('mouseover', function () {
                    var layer = this.getLayer();
                    document.body.style.cursor = 'pointer';
                    this.strokeWidth(4);
                    layer.draw();
                });
                anchor.on('mouseout', function () {
                    var layer = this.getLayer();
                    document.body.style.cursor = 'default';
                    this.strokeWidth(2);
                    layer.draw();
                });

                group.add(anchor);
            }

            var stage = null;
            var layer = null;
            function loadKonva() {
                stage = new Konva.Stage({
                    container: 'container',
                    width,
                    height: height * pdfImage.length
                });

                layer = new Konva.Layer();
                stage.add(layer);

                const canvas = layer.getCanvas()._canvas;
                canvas.id = 'konvaCanvas';

                var tr = new Konva.Transformer();
                layer.add(tr);
            }

            function showAnchors() {
                this.find('Circle').show();
                layer.draw();
            }

            function hideAnchors() {
                this.find('Circle').hide();
                layer.draw();
            }

            function addImage(dataImage, x, y, width, height, resize = false) {
                var imageObj = new Image();
                imageObj.onload = function () {
                    var img = new Konva.Image({
                        x,
                        y,
                        image: imageObj,
                        width,
                        height
                    });

                    layer.add(img);
                    layer.batchDraw();

                    if (resize) {
                        var group = new Konva.Group({
                            x: 180,
                            y: 50,
                            draggable: true,
                        });

                        group.on('mouseenter', showAnchors);
                        group.on('mouseleave', hideAnchors);

                        layer.add(group);
                        group.add(img);

                        addAnchor(group, 0, 0, 'topLeft');
                        addAnchor(group, width, 0, 'topRight');
                        addAnchor(group, width, height, 'bottomRight');
                        addAnchor(group, 0, height, 'bottomLeft');
                    }
                };
                imageObj.src = dataImage;
            }

            function loadImagePDF() {
                pdfImage = pdfImage.sort(function(a, b) {return a.position - b.position})
                pdfImage.forEach((x, i) => {
                    addImage(x.img, 0, i * height, width, height, false)
                })
            }

            function getImageURL(imgData, width, height) {
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.putImageData(imgData, 0, 0);
                return canvas.toDataURL('image/jpeg', 1);
            }

            function pixelsToMM(pixel) {
                return Math.floor(pixel * 0.264583);
            }

            let formDataSend = null;
            $("#savePdf").on('click', function() {
                var canvasContainer = document.getElementById('konvaCanvas');
                var canvasContext = canvasContainer.getContext('2d');
                var pdf = new jsPDF('p', 'mm', 'a4');
                pdfCreatedImage = [];

                for (var i = 0; i < pdfImage.length; i++) {
                    var imageData = canvasContext.getImageData(0, height * i, width, height);
                    pdfCreatedImage.push(getImageURL(imageData, width, height));
                }

                $("#savePdf").attr("disabled", true)

                for (var i = 0; i < pdfCreatedImage.length; i++) {
                    if (i != 0) {
                        pdf.addPage();
                    }
                    pdf.setPage(i + 1);
                    pdf.addImage(pdfCreatedImage[i], 'JPEG', 0, 0, pixelsToMM(width), pixelsToMM(height));
                }

                setTimeout(() => {
                    formDataSend = new FormData();
                    formDataSend.append('pdfOutput', pdf.output('blob'));
                    formDataSend.append('formId', {{$form->id}});

                    $.ajax({
                        cache: false,
                        contentType: false,
                        processData: false,
                        url: '{{route('step2Process', [$form->id])}}',
                        type: 'POST',
                        data: formDataSend,
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        success: function(result) {
                            $("#savePdf").attr("disabled", false)
                            window.location.href = '{{ route("step3", [$form->id]) }}';
                            return false;
                        },
                        error: function(result) {
                            $("#savePdf").attr("disabled", false)
                        }
                    });
                }, pdfImage.length * 300)
            })

        </script>
    </body>
</html>
